package com.ellendan.mss.integration.order.rpc.service;

import com.ellendan.mss.integration.order.common.domain.Order;
import com.ellendan.mss.integration.order.common.domain.Storage;
import com.ellendan.mss.integration.order.common.infrastructure.client.InventoryFeignClient;
import com.ellendan.mss.integration.order.common.service.OrderApplicationService;
import com.ellendan.mss.integration.order.common.service.OrderCreateCommand;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service
public class OrderApplicationServiceImpl implements OrderApplicationService {
    private final InventoryFeignClient inventoryFeignClient;

    @Override
    public Order createOrder(OrderCreateCommand orderCommand) {
        throw new RuntimeException("No support method");
    }

    @Override
    public Storage readStorage(String productId) {
        return inventoryFeignClient.fetchLatestCount(productId);
    }
}
