package com.ellendan.mss.integration.order;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients
@EntityScan
@SpringBootApplication
public class RpcApp {

    public static void main(String[] args) {
        SpringApplication.run(RpcApp.class, args);
    }
}
