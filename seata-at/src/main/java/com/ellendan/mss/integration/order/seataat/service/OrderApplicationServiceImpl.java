package com.ellendan.mss.integration.order.seataat.service;

import com.ellendan.mss.integration.order.common.domain.Order;
import com.ellendan.mss.integration.order.common.domain.OrderStatus;
import com.ellendan.mss.integration.order.common.domain.Storage;
import com.ellendan.mss.integration.order.common.infrastructure.client.InventoryFeignClient;
import com.ellendan.mss.integration.order.common.infrastructure.client.MinusStorageRequest;
import com.ellendan.mss.integration.order.common.infrastructure.jpa.OrderRepository;
import com.ellendan.mss.integration.order.common.service.OrderApplicationService;
import com.ellendan.mss.integration.order.common.service.OrderCreateCommand;
import io.seata.spring.annotation.GlobalTransactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.UUID;

@Slf4j
@Service
public class OrderApplicationServiceImpl implements OrderApplicationService {
    private final InventoryFeignClient inventoryFeignClient;
    private final OrderRepository orderRepository;

    public OrderApplicationServiceImpl(InventoryFeignClient inventoryFeignClient,
                                       OrderRepository orderRepository) {
        this.inventoryFeignClient = inventoryFeignClient;
        this.orderRepository = orderRepository;
    }

    @Override
    @GlobalTransactional
    @Transactional(rollbackFor = Exception.class)
    public Order createOrder(OrderCreateCommand command) {
        Order newOrder = Order.builder()
                .id(UUID.randomUUID().toString())
                .productId(command.getProductId())
                .quantity(command.getQuantity())
                .orderStatus(OrderStatus.CREATED)
                .build();
        newOrder = orderRepository.save(newOrder);
        inventoryFeignClient.minusStorage(command.getProductId(),
                new MinusStorageRequest(command.getProductId(), command.getQuantity())
        );
        if (System.currentTimeMillis() % 3 == 0) {
            log.error("Random rollback");
            throw new RuntimeException("Random rollback order created");
        }
        return newOrder;
    }

    @Override
    public Storage readStorage(String productId) {
        return inventoryFeignClient.fetchLatestCount(productId);
    }
}
