package com.ellendan.mss.integration.order.mqsub.infrastructure.jpa;

import com.ellendan.mss.integration.order.mqsub.domain.EventMessage;
import com.ellendan.mss.integration.order.mqsub.domain.Status;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Set;

public interface EventMessageRepository extends JpaRepository<EventMessage, Long> {
    @Query("from EventMessage em where em.status in (:statusSet) order by em.createdTime asc")
    List<EventMessage> findByStatusAndOrderByCreatedTimeAsc(Set<Status> statusSet);
}
