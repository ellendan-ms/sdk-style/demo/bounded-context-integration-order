package com.ellendan.mss.integration.order.mqsub.service;

import com.ellendan.mss.integration.order.common.domain.Order;
import com.ellendan.mss.integration.order.common.domain.OrderStatus;
import com.ellendan.mss.integration.order.common.service.OrderCreateCommand;
import com.ellendan.mss.integration.order.mqsub.domain.OrderCreatedEvent;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.UUID;

@Service
public class OrderFactory {
    public Pair<Order, OrderCreatedEvent> createOrderFromCommand(OrderCreateCommand command) {
        Order newOrder = Order.builder()
                .id(UUID.randomUUID().toString())
                .productId(command.getProductId())
                .quantity(command.getQuantity())
                .orderStatus(OrderStatus.CREATED)
                .createdTime(LocalDateTime.now())
                .build();

        OrderCreatedEvent orderCreatedEvent = OrderCreatedEvent.builder()
                .aggregationId(newOrder.getId())
                .occurrenceOn(newOrder.getCreatedTime())
                .productId(newOrder.getProductId())
                .quantity(newOrder.getQuantity())
                .build();

        return Pair.of(newOrder, orderCreatedEvent);
    }
}
