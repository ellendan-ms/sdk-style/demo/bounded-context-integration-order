package com.ellendan.mss.integration.order.mqsub.domain;

import java.time.LocalDateTime;

public interface DomainEvent<ID> {
    ID getAggregationId();
    LocalDateTime getOccurrenceOn();
    Boolean needRemoteSent();
}
