package com.ellendan.mss.integration.order.mqsub.domain;

public enum Status {
    CREATED, PUBLISHED, PUBLISH_FAILED
}
