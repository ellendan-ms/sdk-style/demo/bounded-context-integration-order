package com.ellendan.mss.integration.order.mqsub.interfaces;

import com.ellendan.mss.integration.order.mqsub.domain.EventMessage;
import com.ellendan.mss.integration.order.mqsub.domain.Status;
import com.ellendan.mss.integration.order.mqsub.infrastructure.jpa.EventMessageRepository;
import com.ellendan.mss.integration.order.mqsub.service.EventPublisher;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static com.google.common.collect.Sets.newHashSet;

@Service
@RestController
@RequiredArgsConstructor
@RequestMapping("/compensation-messages")
public class CompensationSendMessageController {
    private final EventMessageRepository eventMessageRepository;
    private final EventPublisher eventPublisher;

    @GetMapping
    public void triggerResendMessage() {
        List<EventMessage> eventMessageList = eventMessageRepository.findByStatusAndOrderByCreatedTimeAsc(
                newHashSet(Status.CREATED, Status.PUBLISH_FAILED));
        eventMessageList.forEach(eventPublisher::republish);
    }
}
