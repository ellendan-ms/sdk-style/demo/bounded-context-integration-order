package com.ellendan.mss.integration.order.mqsub.service;

import com.ellendan.mss.integration.order.common.domain.Order;
import com.ellendan.mss.integration.order.common.domain.Storage;
import com.ellendan.mss.integration.order.common.infrastructure.client.InventoryFeignClient;
import com.ellendan.mss.integration.order.common.infrastructure.jpa.OrderRepository;
import com.ellendan.mss.integration.order.common.service.OrderApplicationService;
import com.ellendan.mss.integration.order.common.service.OrderCreateCommand;
import com.ellendan.mss.integration.order.mqsub.domain.OrderCreatedEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import org.redisson.api.RAtomicLong;
import org.redisson.api.RLock;
import org.redisson.api.RReadWriteLock;
import org.redisson.api.RedissonClient;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.concurrent.TimeUnit;

@Slf4j
@RequiredArgsConstructor
@Service
public class OrderApplicationServiceImpl implements OrderApplicationService {
    private final InventoryFeignClient inventoryFeignClient;
    private final RedissonClient redissonClient;
    private final OrderRepository orderRepository;
    private final OrderFactory orderFactory;
    private final EventPublisher domainEventPublisher;

    @Transactional(rollbackFor = Throwable.class)
    public Order createOrder(OrderCreateCommand orderCommand) {
        RAtomicLong count = redissonClient.getAtomicLong(cacheKeyForStorage(orderCommand.getProductId()));
        if(count.get() == 0) {
            this.initInventoryCount(orderCommand.getProductId());
        }
        count = redissonClient.getAtomicLong(cacheKeyForStorage(orderCommand.getProductId()));
        Long remainCount = count.addAndGet(orderCommand.getQuantity() * -1L);
        if(remainCount < 0) {
            count.getAndAdd(orderCommand.getQuantity());
            throw new RuntimeException("over quantity");
        }

        Pair<Order, OrderCreatedEvent> newOrderPair = orderFactory.createOrderFromCommand(orderCommand);
        try {
            Order newOrder = orderRepository.save(newOrderPair.getLeft());
            domainEventPublisher.publish(newOrderPair.getRight());
            return newOrder;
        } catch (Exception e){
            log.error(e.getMessage(), e);
            count.getAndAdd(orderCommand.getQuantity());
            throw e;
        }
    }

    @Override
    public Storage readStorage(String productId) {
        RAtomicLong count = redissonClient.getAtomicLong(cacheKeyForStorage(productId));
        if(count.get() == 0) {
            this.initInventoryCount(productId);
        }
        count = redissonClient.getAtomicLong(cacheKeyForStorage(productId));
        return Storage.builder()
                .productId(productId)
                .quantity((int)count.get())
                .build();
    }

    private void initInventoryCount(String productId) {
        RReadWriteLock readWriteLock = redissonClient.getReadWriteLock(productId);
        RLock writeLock = readWriteLock.writeLock();
        try {
            writeLock.tryLock(6, 5, TimeUnit.SECONDS);
            Storage productInventory = inventoryFeignClient.fetchLatestCount(productId);
            RAtomicLong count = redissonClient.getAtomicLong(cacheKeyForStorage(productId));
            count.compareAndSet(0, productInventory.getQuantity());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        writeLock.unlock();
    }

    private String cacheKeyForStorage(String productId) {
        return "product-" + productId;
    }

}
