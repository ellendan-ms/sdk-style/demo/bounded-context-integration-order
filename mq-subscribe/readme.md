1. 订单服务初始化库存
```
curl http://localhost:8080/orders/?productId=code-1&init
```

2. 创建订单
```
curl http://localhost:8080/orders --header 'Content-Type: application/json' --data '{"productId": "code-1", "quantity": 2}' 

```

3. 补偿发送订单消息
```
curl http://localhost:8080/compensation-messages
```