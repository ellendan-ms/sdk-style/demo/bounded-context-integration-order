package com.ellendan.mss.integration.order.rpcsub.infrastructure.redisson;

import org.redisson.codec.JsonJacksonCodec;

public class CustomizeJsonJacksonCodec extends JsonJacksonCodec {
    public CustomizeJsonJacksonCodec() {
        super();
        this.getObjectMapper().findAndRegisterModules();
    }
}
