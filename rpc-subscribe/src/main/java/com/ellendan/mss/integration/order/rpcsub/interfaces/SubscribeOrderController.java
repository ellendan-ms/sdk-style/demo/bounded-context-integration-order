package com.ellendan.mss.integration.order.rpcsub.interfaces;

import com.ellendan.mss.integration.order.rpcsub.service.OrderApplicationServiceImpl;
import com.ellendan.mss.integration.order.common.interfaces.OrderResponse;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/subscribe-orders")
public class SubscribeOrderController {
    private final OrderApplicationServiceImpl orderApplicationService;

    public SubscribeOrderController(OrderApplicationServiceImpl orderApplicationService) {
        this.orderApplicationService = orderApplicationService;
    }

    @PostMapping
    List<OrderResponse> subscribeNewOrders(@RequestBody SubscribeOrderRequest subscribeOrderRequest) {
        return Objects.requireNonNull(
                orderApplicationService.getCacheOrders(subscribeOrderRequest.getProductId(),
                        subscribeOrderRequest.getTimestamp(),
                        subscribeOrderRequest.getPageNo(),
                        subscribeOrderRequest.getPageSize()))
                .stream()
                .map(OrderResponse::fromEntity)
                .collect(Collectors.toList());
    }
}
