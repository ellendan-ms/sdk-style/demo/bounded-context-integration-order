package com.ellendan.mss.integration.order.common.infrastructure.client;

import lombok.Data;

@Data
public class MinusStorageRequest {
    private final String productId;
    private final Integer quantity;
}
