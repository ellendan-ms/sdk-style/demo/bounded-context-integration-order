package com.ellendan.mss.integration.order.common.infrastructure.client;

import com.ellendan.mss.integration.order.common.domain.Storage;
import io.github.resilience4j.circuitbreaker.CallNotPermittedException;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.ratelimiter.annotation.RateLimiter;
import io.github.resilience4j.retry.annotation.Retry;
import io.github.resilience4j.timelimiter.annotation.TimeLimiter;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "inventoryClient", url = "127.0.0.1:8081")
public interface InventoryFeignClient {

    @CircuitBreaker(name="get-storage", fallbackMethod = "fallback")
    @Retry(name="get-storage")
    @RateLimiter(name="get-storage")
    @TimeLimiter(name="get-storage")
    @GetMapping("/storages/{productId}")
    Storage fetchLatestCount(@PathVariable("productId") String productId);

    @PutMapping("/storages/{productId}")
    Storage minusStorage(@PathVariable("productId") String productId,
                         @RequestBody MinusStorageRequest minusStorageRequest);

    default Storage fallback(String productId, CallNotPermittedException e) {
        return Storage.builder()
                .productId(productId)
                .quantity(0)
                .build();
    }
}
