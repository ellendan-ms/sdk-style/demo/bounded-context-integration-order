package com.ellendan.mss.integration.order.common.interfaces;

import com.ellendan.mss.integration.order.common.service.OrderCreateCommand;
import lombok.Data;

@Data
public class OrderCreateRequest {
    private String productId;
    private Integer quantity;

    public OrderCreateCommand toCommand() {
        return OrderCreateCommand.builder()
                .productId(productId)
                .quantity(quantity)
                .build();
    }
}
