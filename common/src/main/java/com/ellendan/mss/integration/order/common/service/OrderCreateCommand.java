package com.ellendan.mss.integration.order.common.service;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class OrderCreateCommand {
    private String productId;
    private Integer quantity;
}
