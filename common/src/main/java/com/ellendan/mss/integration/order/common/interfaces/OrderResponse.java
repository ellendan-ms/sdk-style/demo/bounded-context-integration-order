package com.ellendan.mss.integration.order.common.interfaces;

import com.ellendan.mss.integration.order.common.domain.Order;
import lombok.Builder;
import lombok.Data;
import java.time.LocalDateTime;

@Data
@Builder
public class OrderResponse {
    private String id;
    private String productId;
    private Integer quantity;
    private LocalDateTime createdTime;

    public static OrderResponse fromEntity(Order order) {
        return OrderResponse.builder()
                .id(order.getId())
                .productId(order.getProductId())
                .quantity(order.getQuantity())
                .createdTime(order.getCreatedTime())
                .build();
    }
}
