package com.ellendan.mss.integration.order.common.infrastructure.jpa;

import com.ellendan.mss.integration.order.common.domain.Order;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<Order, String> {
}