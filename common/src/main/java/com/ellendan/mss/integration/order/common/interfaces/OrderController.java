package com.ellendan.mss.integration.order.common.interfaces;

import com.ellendan.mss.integration.order.common.domain.Order;
import com.ellendan.mss.integration.order.common.domain.Storage;
import com.ellendan.mss.integration.order.common.service.OrderApplicationService;
import com.ellendan.mss.integration.order.common.service.OrderCreateCommand;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/orders")
public class OrderController {

    private final OrderApplicationService orderApplicationService;

    public OrderController(OrderApplicationService orderApplicationService) {
        this.orderApplicationService = orderApplicationService;
    }

    @PostMapping
    OrderResponse createOrder(@RequestBody OrderCreateRequest request) {
        OrderCreateCommand orderCreateCommand = request.toCommand();
        Order order = this.orderApplicationService.createOrder(orderCreateCommand);
        return OrderResponse.fromEntity(order);
    }

    @GetMapping(params = {"init", "productId"})
    Storage initInventoryCount(@RequestParam("productId") String productId) {
        return orderApplicationService.readStorage(productId);
    }
}
