package com.ellendan.mss.integration.order.common.service;

import com.ellendan.mss.integration.order.common.domain.Order;
import com.ellendan.mss.integration.order.common.domain.Storage;

public interface OrderApplicationService {
    Order createOrder(OrderCreateCommand orderCommand);

    Storage readStorage(String productId);
}
