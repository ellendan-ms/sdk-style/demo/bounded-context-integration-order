package com.ellendan.mss.integration.order.common.domain;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class Storage {
    private String productId;
    private LocalDateTime lastModifiedTime;
    private Integer quantity;
}
