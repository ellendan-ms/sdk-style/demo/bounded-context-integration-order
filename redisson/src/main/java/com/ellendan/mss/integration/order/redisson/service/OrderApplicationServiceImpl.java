package com.ellendan.mss.integration.order.redisson.service;

import com.ellendan.mss.integration.order.common.domain.Order;
import com.ellendan.mss.integration.order.common.domain.OrderStatus;
import com.ellendan.mss.integration.order.common.domain.Storage;
import com.ellendan.mss.integration.order.common.infrastructure.client.InventoryFeignClient;
import com.ellendan.mss.integration.order.common.infrastructure.jpa.OrderRepository;
import com.ellendan.mss.integration.order.common.service.OrderApplicationService;
import com.ellendan.mss.integration.order.common.service.OrderCreateCommand;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RAtomicLong;
import org.redisson.api.RLock;
import org.redisson.api.RReadWriteLock;
import org.redisson.api.RedissonClient;
import org.springframework.stereotype.Service;


import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Slf4j
@RequiredArgsConstructor
@Service
public class OrderApplicationServiceImpl implements OrderApplicationService {
    private final InventoryFeignClient inventoryFeignClient;
    private final RedissonClient redissonClient;
    private final OrderRepository orderRepository;

    @Override
    public Order createOrder(OrderCreateCommand command) {
        RAtomicLong count = redissonClient.getAtomicLong(cacheKeyForStorage(command.getProductId()));
        if(count.get() == 0) {
            this.initInventoryCount(command.getProductId());
        }
        count = redissonClient.getAtomicLong(cacheKeyForStorage(command.getProductId()));
        Long remainCount = count.addAndGet(command.getQuantity() * -1L);
        if(remainCount < 0) {
            count.getAndAdd(command.getQuantity());
            throw new RuntimeException("over quantity");
        }
        try {
            Order newOrder = Order.builder()
                    .id(UUID.randomUUID().toString())
                    .productId(command.getProductId())
                    .quantity(command.getQuantity())
                    .orderStatus(OrderStatus.CREATED)
                    .build();
            return orderRepository.save(newOrder);
        } catch (Exception e){
            log.error(e.getMessage(), e);
            count.getAndAdd(command.getQuantity());
            throw e;
        }
    }

    @Override
    public Storage readStorage(String productId) {
        RAtomicLong count = redissonClient.getAtomicLong(cacheKeyForStorage(productId));
        if(count.get() == 0) {
            this.initInventoryCount(productId);
        }
        count = redissonClient.getAtomicLong(cacheKeyForStorage(productId));
        return Storage.builder()
                .productId(productId)
                .quantity((int)count.get())
                .build();
    }

    private void initInventoryCount(String productId) {
        RReadWriteLock readWriteLock = redissonClient.getReadWriteLock(productId);
        RLock writeLock = readWriteLock.writeLock();
        try {
            writeLock.tryLock(6, 5, TimeUnit.SECONDS);
            Storage productInventory = inventoryFeignClient.fetchLatestCount(productId);
            RAtomicLong count = redissonClient.getAtomicLong(cacheKeyForStorage(productId));
            count.compareAndSet(0, productInventory.getQuantity());
        } catch (InterruptedException e) {
            log.error(e.getMessage(), e);
        }
        writeLock.unlock();
    }

    private String cacheKeyForStorage(String productId) {
        return "product-" + productId;
    }

}
