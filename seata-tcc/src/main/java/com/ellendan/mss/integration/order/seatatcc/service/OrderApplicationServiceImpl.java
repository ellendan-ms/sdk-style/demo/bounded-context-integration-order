package com.ellendan.mss.integration.order.seatatcc.service;

import com.ellendan.mss.integration.order.common.domain.Order;
import com.ellendan.mss.integration.order.common.domain.Storage;
import com.ellendan.mss.integration.order.common.infrastructure.client.InventoryFeignClient;
import com.ellendan.mss.integration.order.common.infrastructure.client.MinusStorageRequest;
import com.ellendan.mss.integration.order.common.service.OrderApplicationService;
import com.ellendan.mss.integration.order.common.service.OrderCreateCommand;
import io.seata.spring.annotation.GlobalTransactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class OrderApplicationServiceImpl implements OrderApplicationService {
    private final OrderCreateTccService orderCreateService;
    private final InventoryFeignClient inventoryFeignClient;

    @GlobalTransactional
    public Order createOrder(OrderCreateCommand command) {
        Order newOrder = orderCreateService.prepare(null, command);
        inventoryFeignClient.minusStorage(command.getProductId(),
                new MinusStorageRequest(newOrder.getProductId(), newOrder.getQuantity()));
        return newOrder;
    }

    @Override
    public Storage readStorage(String productId) {
        return null;
    }
}
